const express = require('express');
const productController = require('../controllers/productController');
const auth = require('../middleware/auth');

const router = express.Router();

// Routes for Fetch All product Details
router.get('/',auth, productController.getAllProducts);

module.exports = router;
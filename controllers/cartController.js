const Cart = require('../models/cartModel');

// Add product to the cart
async function addToCart(req, res) {
  const { productId, quantity } = req.body;
  try {
    let cart = await Cart.findOne({ user: req.user.id });
    if (!cart) {
      cart = new Cart({ user: req.user.id, products: [] });
    }

    const productIndex = cart.products.findIndex(p => p.product.toString() === productId);
    if (productIndex >= 0) {
      cart.products[productIndex].quantity += quantity;
    } else {
      cart.products.push({ product: productId, quantity });
    }

    await cart.save();
    res.json(cart);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
};


// Remove Product from cart
async function removeFromCart(req, res) {
  const { productId } = req.body;
  try {
    let cart = await Cart.findOne({ user: req.user.id });
    if (!cart) {
      return res.status(400).json({ msg: 'Cart not found' });
    }

    cart.products = cart.products.filter(p => p.product.toString() !== productId);

    await cart.save();
    res.json(cart);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
};

module.exports = { addToCart,removeFromCart }
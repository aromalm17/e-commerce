const express = require('express');
const cartController = require('../controllers/cartController');
const auth = require('../middleware/auth');

const router = express.Router();

// Add To cart route
router.post('/add', auth, cartController.addToCart);

// Remove from cart route
router.post('/remove', auth, cartController.removeFromCart);

module.exports = router;